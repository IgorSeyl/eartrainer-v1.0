package main.models;

import javafx.util.Pair;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.FIELD)
public class Result {

    private Map<String, Pair<Integer, Integer>> result;

    private static Result instance;

    private Result() {
        this.result = new HashMap<>();
    }

    public static Result getInstance() {
        if (instance == null){
            instance = new Result();
        }

        return instance;
    }

    public Map<String, Pair<Integer, Integer>> getResult() {
        return result;
    }

    public void setResult(Map<String, Pair<Integer, Integer>> result) {
        this.result = result;
    }

    public void setResult(String degree, Integer correctAnswers, Integer questions) {
        this.result.put(degree, new Pair<>(correctAnswers, questions));
    }

}
