package main.models;

import javafx.scene.control.Button;

public class PrevButton {

    private static Button instance;

    private PrevButton() {
    }

    public static Button getInstance() {
        if (instance == null) {
            instance = new Button("Назад");
        }

        return instance;
    }
}
