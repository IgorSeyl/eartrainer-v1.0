package main.models;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import main.helpers.Helper;

import java.io.File;
import java.util.ArrayList;

public class Sound extends Thread {

    private ArrayList<MediaPlayer> players;
    private static Sound instance;

    private Sound() {
        this.players = new ArrayList<>();
    }

    public static Sound getInstance() {
        if (instance == null) {
            instance = new Sound();
        }

        return instance;
    }

    public ArrayList<MediaPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<MediaPlayer> players) {
        this.players = players;
    }

    public static void play(String... wavFiles) {

        Sound sound = Sound.getInstance();

        ArrayList<Media> medias = new ArrayList<>();

        for (int i = 0; i < sound.getPlayers().size(); i++) {
            sound.getPlayers().get(i).stop();
        }

        sound.getPlayers().clear();

        for (int i = 0; i < wavFiles.length; i++) {

            File file = new File(Helper.checkOS(wavFiles[i]));
            medias.add(new Media(file.toURI().toString()));
            sound.getPlayers().add(new MediaPlayer(medias.get(i)));
        }

        for (int i = 0; i < wavFiles.length; i++) {
            int finalI = i;
            if (i != 0)
                sound.getPlayers().get(finalI - 1).setOnEndOfMedia(() -> sound.getPlayers().get(finalI).play());
        }

        sound.getPlayers().get(0).play();
    }
}
