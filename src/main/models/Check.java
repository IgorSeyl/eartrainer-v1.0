package main.models;

import java.util.concurrent.atomic.AtomicBoolean;

public class Check {

    private boolean isMelody;
    private AtomicBoolean incorrectAnswer;

    private static Check instance;

    private Check(){

        this.isMelody = true;
        this.incorrectAnswer = new AtomicBoolean(false);
    }

    public static Check getInstance() {
        if (instance == null){
            instance = new Check();
        }

        return instance;
    }

    public boolean isMelody() {
        return isMelody;
    }

    public void setMelody(boolean melody) {
        isMelody = melody;
    }

    public AtomicBoolean getIncorrectAnswer() {
        return incorrectAnswer;
    }

    public void setIncorrectAnswer(AtomicBoolean incorrectAnswer) {
        this.incorrectAnswer = incorrectAnswer;
    }
}
