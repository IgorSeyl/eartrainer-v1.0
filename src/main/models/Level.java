package main.models;

import main.enums.Inversion;
import main.enums.Key;
import main.enums.Tonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.HashSet;

@XmlRootElement(name = "level")
@XmlAccessorType(XmlAccessType.FIELD)
public class Level {

    private int numOfQuestions;
    private boolean isMinor;

    private Tonic tonic;
    private HashSet<String> enabledDegrees;

    private Key key;
    private Inversion enabledInversions;

    @XmlTransient
    private String filePath;

    public Level() {
    }

    public Level(int numOfQuestions, Tonic tonic, boolean isMinor, HashSet<String> enabledDegrees) {
        this.numOfQuestions = numOfQuestions;
        this.tonic = tonic;
        this.isMinor = isMinor;
        this.enabledDegrees = enabledDegrees;
    }

    public Level(
            int numOfQuestions, HashSet<String> enabledDegrees, Inversion enabledInversions, boolean isMinor, Key key) {
        this.numOfQuestions = numOfQuestions;
        this.enabledDegrees = enabledDegrees;
        this.enabledInversions = enabledInversions;
        this.isMinor = isMinor;
        this.key = key;
    }

    public int getNumOfQuestions() {
        return numOfQuestions;
    }

    public void setNumOfQuestions(int numOfQuestions) {
        this.numOfQuestions = numOfQuestions;
    }

    public boolean isMinor() {
        return isMinor;
    }

    public void setMinor(boolean minor) {
        isMinor = minor;
    }

    public Tonic getTonic() {
        return tonic;
    }

    public void setTonic(Tonic tonic) {
        this.tonic = tonic;
    }

    public HashSet<String> getEnabledDegrees() {
        return enabledDegrees;
    }

    public void setEnabledDegrees(HashSet<String> enabledDegrees) {
        this.enabledDegrees = enabledDegrees;
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Inversion getEnabledInversions() {
        return enabledInversions;
    }

    public void setEnabledInversions(Inversion enabledInversions) {
        this.enabledInversions = enabledInversions;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
