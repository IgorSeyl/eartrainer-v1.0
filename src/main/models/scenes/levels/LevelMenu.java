package main.models.scenes.levels;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import main.models.Level;
import main.models.scenes.other.Settings;

public class LevelMenu {

    private TableView<Level> tblLevels;

    private Button btnChooseLvl;
    private Button btnDeleteLvl;
    private Button btnCreateLevelMenu;

    private GridPane gpLevelMenu;
    private Scene scnLevelMenu;

    private static LevelMenu instance;

    private LevelMenu() {
        this.btnChooseLvl = new Button("Выбрать");
        this.btnDeleteLvl = new Button("Удалить");
        this.btnCreateLevelMenu = new Button("Создать уровень");
        this.tblLevels = new TableView<>();
        this.gpLevelMenu = new GridPane();
        this.scnLevelMenu = new Scene(gpLevelMenu, Settings.getInstance().getWidth(), Settings.getInstance().getHeight());
    }

    public static LevelMenu getInstance() {
        if (instance == null) {
            instance = new LevelMenu();
        }

        return instance;
    }

    public TableView<Level> getTblLevels() {
        return tblLevels;
    }

    public void setTblLevels(TableView<Level> tblLevels) {
        this.tblLevels = tblLevels;
    }

    public Button getBtnChooseLvl() {
        return btnChooseLvl;
    }

    public void setBtnChooseLvl(Button btnChooseLvl) {
        this.btnChooseLvl = btnChooseLvl;
    }

    public Button getBtnDeleteLvl() {
        return btnDeleteLvl;
    }

    public void setBtnDeleteLvl(Button btnDeleteLvl) {
        this.btnDeleteLvl = btnDeleteLvl;
    }

    public Button getBtnCreateLevelMenu() {
        return btnCreateLevelMenu;
    }

    public void setBtnCreateLevelMenu(Button btnCreateLevelMenu) {
        this.btnCreateLevelMenu = btnCreateLevelMenu;
    }

    public GridPane getGpLevelMenu() {
        return gpLevelMenu;
    }

    public void setGpLevelMenu(GridPane gpLevelMenu) {
        this.gpLevelMenu = gpLevelMenu;
    }

    public Scene getScnLevelMenu() {
        return scnLevelMenu;
    }

    public void setScnLevelMenu(Scene scnLevelMenu) {
        this.scnLevelMenu = scnLevelMenu;
    }

    public static void setInstance(LevelMenu instance) {
        LevelMenu.instance = instance;
    }
}
