package main.models.scenes.levels;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import main.enums.Inversion;
import main.enums.Key;
import main.enums.Tonic;
import main.helpers.Helper;
import main.helpers.HelperFX;
import main.models.Check;
import main.models.Level;
import main.models.scenes.other.Settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class PracticeMenu {

    private Button btnRepeat;
    private Button btnShowAnswer;
    private BorderPane bpRepeatAndAnswer;

    private Button btn1;
    private Button btnB2;
    private Button btn2;
    private Button btnB3;
    private Button btn3;
    private Button btn4;
    private Button btnS4;
    private Button btn5;
    private Button btnB6;
    private Button btn6;
    private Button btnB7;
    private Button btn7;
    private Button btn8;
    private ArrayList<Button> buttons;

    private Rectangle rectangle1;
    private Rectangle rectangleB2;
    private Rectangle rectangle2;
    private Rectangle rectangleB3;
    private Rectangle rectangle3;
    private Rectangle rectangle4;
    private Rectangle rectangleS4;
    private Rectangle rectangle5;
    private Rectangle rectangleB6;
    private Rectangle rectangle6;
    private Rectangle rectangleB7;
    private Rectangle rectangle7;
    private Rectangle rectangle8;
    private ArrayList<Rectangle> rectangles;

    private AtomicReference<Level> level;

    private AtomicInteger currentQ;
    private Text txtCurrentAndMaxQ;

    private AtomicInteger numOfCorrectQ;
    private AtomicInteger numOfIncurrentQ;
    private Text txtCorrectAndIncorrectQ;

    private Text txtAnswer;

    private AtomicInteger randomDegree;
    private Key[] randomKey;
    private AtomicInteger randomInversion;

    private GridPane gpPracticeMenu;
    private Scene scnPracticeMenu;

    private static PracticeMenu instance;

    private PracticeMenu() {

        this.btnRepeat = new Button("Повторить вопрос");
        this.btnShowAnswer = new Button("Показать ответ");
        this.bpRepeatAndAnswer = new BorderPane();

        this.btn1 = new Button("1");
        this.btnB2 = new Button("b2");
        this.btn2 = new Button("2");
        this.btnB3 = new Button("b3");
        this.btn3 = new Button("3");
        this.btn4 = new Button("4");
        this.btnS4 = new Button("#4");
        this.btn5 = new Button("5");
        this.btnB6 = new Button("b6");
        this.btn6 = new Button("6");
        this.btnB7 = new Button("b7");
        this.btn7 = new Button("7");
        this.btn8 = new Button("1");
        this.buttons = new ArrayList<>(
                Arrays.asList(btn1, btnB2, btn2, btnB3, btn3, btn4, btnS4, btn5, btnB6, btn6, btnB7, btn7, btn8));

        this.rectangle1 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangleB2 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangle2 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangleB3 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangle3 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangle4 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangleS4 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangle5 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangleB6 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangle6 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangleB7 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangle7 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangle8 = new Rectangle(37.5, 5, Paint.valueOf("green"));
        this.rectangles = new ArrayList<>(
                Arrays.asList(rectangle1, rectangleB2, rectangle2, rectangleB3, rectangle3, rectangle4, rectangleS4,
                        rectangle5, rectangleB6, rectangle6, rectangleB7, rectangle7, rectangle8));

        this.level = new AtomicReference<>();

        this.currentQ = new AtomicInteger(1);
        this.txtCurrentAndMaxQ = new Text("1 / 20");

        this.numOfCorrectQ = new AtomicInteger(0);
        this.numOfIncurrentQ = new AtomicInteger(0);
        this.txtCorrectAndIncorrectQ = new Text("0+ / 0-");

        this.txtAnswer = new Text("Верно!");

        this.randomDegree = new AtomicInteger();
        this.randomKey = new Key[]{null};
        this.randomInversion = new AtomicInteger();

        this.gpPracticeMenu = new GridPane();
        this.scnPracticeMenu = new Scene(gpPracticeMenu,
                Settings.getInstance().getWidth(), Settings.getInstance().getHeight());
    }

    public static PracticeMenu getInstance() {
        if (instance == null) {
            instance = new PracticeMenu();
        }

        return instance;
    }

    public Button getBtnRepeat() {
        return btnRepeat;
    }

    public Button getBtnShowAnswer() {
        return btnShowAnswer;
    }

    public BorderPane getBpRepeatAndAnswer() {
        return bpRepeatAndAnswer;
    }

    public Button getBtn1() {
        return btn1;
    }

    public Button getBtnB2() {
        return btnB2;
    }

    public Button getBtn2() {
        return btn2;
    }

    public Button getBtnB3() {
        return btnB3;
    }

    public Button getBtn3() {
        return btn3;
    }

    public Button getBtn4() {
        return btn4;
    }

    public Button getBtnS4() {
        return btnS4;
    }

    public Button getBtn5() {
        return btn5;
    }

    public Button getBtnB6() {
        return btnB6;
    }

    public Button getBtn6() {
        return btn6;
    }

    public Button getBtnB7() {
        return btnB7;
    }

    public Button getBtn7() {
        return btn7;
    }

    public Button getBtn8() {
        return btn8;
    }

    public ArrayList<Button> getButtons() {
        return buttons;
    }

    public Rectangle getRectangle1() {
        return rectangle1;
    }

    public Rectangle getRectangleB2() {
        return rectangleB2;
    }

    public Rectangle getRectangle2() {
        return rectangle2;
    }

    public Rectangle getRectangleB3() {
        return rectangleB3;
    }

    public Rectangle getRectangle3() {
        return rectangle3;
    }

    public Rectangle getRectangle4() {
        return rectangle4;
    }

    public Rectangle getRectangleS4() {
        return rectangleS4;
    }

    public Rectangle getRectangle5() {
        return rectangle5;
    }

    public Rectangle getRectangleB6() {
        return rectangleB6;
    }

    public Rectangle getRectangle6() {
        return rectangle6;
    }

    public Rectangle getRectangleB7() {
        return rectangleB7;
    }

    public Rectangle getRectangle7() {
        return rectangle7;
    }

    public Rectangle getRectangle8() {
        return rectangle8;
    }

    public ArrayList<Rectangle> getRectangles() {
        return rectangles;
    }

    public AtomicReference<Level> getLevel() {
        return level;
    }

    public void setLevel(AtomicReference<Level> level) {
        this.level = level;
    }

    public Text getTxtCurrentAndMaxQ() {
        return txtCurrentAndMaxQ;
    }

    private void setTxtCurrentAndMaxQ(AtomicInteger currentQ, int maxQ) {
        getTxtCurrentAndMaxQ().setText(currentQ.get() + " / " + maxQ);
    }

    public AtomicInteger getCurrentQ() {
        return currentQ;
    }

    public void setCurrentQ(AtomicInteger num) {
        this.currentQ = num;
        setTxtCurrentAndMaxQ(getCurrentQ(), getMaxQ());
    }

    public void setCurrentQ(int num) {
        this.currentQ.set(num);
        setTxtCurrentAndMaxQ(getCurrentQ(), getMaxQ());
    }

    public void increaseCurrentQ() {
        this.currentQ.set(getCurrentQ().get() + 1);
        setTxtCurrentAndMaxQ(getCurrentQ(), getMaxQ());
    }

    public int getMaxQ() {
        return level.get().getNumOfQuestions();
    }

    public Text getTxtCorrectAndIncorrectQ() {
        return txtCorrectAndIncorrectQ;
    }

    private void setTxtCorrectAndIncorrectQ(AtomicInteger correctQ, AtomicInteger incorrectQ) {
        getTxtCorrectAndIncorrectQ().setText(correctQ.get() + "+ / " + incorrectQ.get() + "-");
    }

    public AtomicInteger getNumOfCorrectQ() {
        return numOfCorrectQ;
    }

    public void setNumOfCorrectQ(AtomicInteger num) {
        this.numOfCorrectQ = num;
        setTxtCorrectAndIncorrectQ(getNumOfCorrectQ(), getNumOfIncorrectQ());
    }

    public void setNumOfCorrectQ(int num) {
        this.numOfCorrectQ.set(num);
        setTxtCorrectAndIncorrectQ(getNumOfCorrectQ(), getNumOfIncorrectQ());
    }

    public void increaseNumOfCorrectQ() {
        this.numOfCorrectQ.set(getNumOfCorrectQ().get() + 1);
        setTxtCorrectAndIncorrectQ(getNumOfCorrectQ(), getNumOfIncorrectQ());
    }

    public AtomicInteger getNumOfIncorrectQ() {
        return numOfIncurrentQ;
    }

    public void setNumOfIncorrectQ(AtomicInteger num) {
        this.numOfIncurrentQ = num;
        setTxtCorrectAndIncorrectQ(getNumOfCorrectQ(), getNumOfIncorrectQ());
    }

    public void setNumOfIncorrectQ(int num) {
        this.numOfIncurrentQ.set(num);
        setTxtCorrectAndIncorrectQ(getNumOfCorrectQ(), getNumOfIncorrectQ());
    }

    public void increaseNumOfIncorrectQ() {
        this.numOfIncurrentQ.set(getNumOfIncorrectQ().get() + 1);
        setTxtCorrectAndIncorrectQ(getNumOfCorrectQ(), getNumOfIncorrectQ());
    }

    public Text getTxtAnswer() {
        return txtAnswer;
    }

    public void setTxtAnswer(boolean answer) {
        getTxtAnswer().setText(answer ? "Верно!" : "Не верно.");
        getTxtAnswer().setFill(Paint.valueOf(answer ? "green" : "red"));
        HelperFX.reveal(getTxtAnswer(), 100, 1500);
    }

    public AtomicInteger getRandomDegree() {
        return randomDegree;
    }

    public void setRandomDegree(AtomicInteger randomDegree) {
        this.randomDegree = randomDegree;
    }

    public void setRandomDegree(HashSet<String> degrees) {

        List<Integer> integerList = new ArrayList<>();

        if (Check.getInstance().isMelody()) {

            for (String degree : degrees) {
                switch (degree) {
                    case "1":
                        integerList.add(1);
                        break;
                    case "b2":
                        integerList.add(2);
                        break;
                    case "2":
                        integerList.add(3);
                        break;
                    case "b3":
                        integerList.add(4);
                        break;
                    case "3":
                        integerList.add(5);
                        break;
                    case "4":
                        integerList.add(6);
                        break;
                    case "#4":
                        integerList.add(7);
                        break;
                    case "5":
                        integerList.add(8);
                        break;
                    case "b6":
                        integerList.add(9);
                        break;
                    case "6":
                        integerList.add(10);
                        break;
                    case "b7":
                        integerList.add(11);
                        break;
                    case "7":
                        integerList.add(12);
                        break;
                    case "1+":
                        integerList.add(13);
                        break;
                }
            }

        } else for (String degree : degrees) integerList.add(Integer.parseInt(degree));

        int enabledDegreesSize = PracticeMenu.getInstance().getLevel().get().getEnabledDegrees().size();

        this.randomDegree = new AtomicInteger(integerList.get(Helper.getRandom(0, enabledDegreesSize - 1)));
    }

    public Key[] getRandomKey() {
        return randomKey;
    }

    public void setRandomKey(Key randomKey) {
        this.randomKey[0] = randomKey;
    }

    public void setRandomKey(Tonic tonic) {

        Key resultKey = Key.C;

        int min = 0;
        int max = 11;
        final ObservableList<Key> keys = FXCollections.observableArrayList(Key.values());

        switch (tonic) {
            case NOT_C:
                keys.remove(Key.C);
                max = max - 1;
            case RANDOM:
                resultKey = keys.get(Helper.getRandom(min, max));
                break;
        }

        this.randomKey[0] = resultKey;
    }

    public AtomicInteger getRandomInversion() {
        return randomInversion;
    }

    public void setRandomInversion(AtomicInteger randomInversion) {
        this.randomInversion = randomInversion;
    }

    public void setRandomInversion(Inversion inversion) {

        AtomicInteger resultInteger = new AtomicInteger(1);

        switch (inversion) {
            case OFF:
                resultInteger = new AtomicInteger(1);
                break;
            case FIRST:
                resultInteger = new AtomicInteger(2);
                break;
            case SECOND:
                resultInteger = new AtomicInteger(3);
                break;
            case RANDOM:
                resultInteger = new AtomicInteger(Helper.getRandom(1, 3));
                break;
        }

        this.randomInversion = resultInteger;
    }

    public GridPane getGpPracticeMenu() {
        return gpPracticeMenu;
    }

    public Scene getScnPracticeMenu() {
        return scnPracticeMenu;
    }

}
