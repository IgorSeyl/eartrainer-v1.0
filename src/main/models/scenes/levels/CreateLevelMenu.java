package main.models.scenes.levels;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import main.models.scenes.other.Settings;

public class CreateLevelMenu {

    private Text txtNumOfQuestions;
    private Slider sldrNumOfQuestions;

    private Text txtKeyType;
    private RadioButton rbMajor;
    private RadioButton rbMinor;
    private ToggleGroup tgKeyType;

    private Text txtTonic;
    private RadioButton rbC;
    private RadioButton rbNotC;
    private RadioButton rbRandom;
    private ToggleGroup tgTonic;
    private Slider sldrTonic;

    private Text txtTones;

    private CheckBox cbTone1;
    private CheckBox cbTone2;
    private CheckBox cbTone3;
    private CheckBox cbTone4;
    private CheckBox cbTone5;
    private CheckBox cbTone6;
    private CheckBox cbTone7;
    private CheckBox cbTone8;
    private CheckBox cbTone9;
    private CheckBox cbTone10;
    private CheckBox cbTone11;
    private CheckBox cbTone12;
    private CheckBox cbTone13;

    private Text txtTone1;
    private Text txtToneB2;
    private Text txtTone2;
    private Text txtToneB3;
    private Text txtTone3;
    private Text txtTone4;
    private Text txtToneS4;
    private Text txtTone5;
    private Text txtToneB6;
    private Text txtTone6;
    private Text txtToneB7;
    private Text txtTone7;
    private Text txtTone8;

    private GridPane gpTones;

    private Text txtChordsEnabled;
    private CheckBox cbChord1;
    private CheckBox cbChord2;
    private CheckBox cbChord3;
    private CheckBox cbChord4;
    private CheckBox cbChord5;
    private CheckBox cbChord6;
    private CheckBox cbChord7;

    private HBox hbChordsEnabled;

    private Text txtInversions;
    private RadioButton rbInvOff;
    private RadioButton rbInv1st;
    private RadioButton rbInv2nd;
    private RadioButton rbInvRandom;
    private ToggleGroup tgInversions;

    private Button btnCreateLevel;
    private Text txtCreateError;

    private GridPane gpCreateLevel;

    private Scene scnCreateLevelMenu;

    private static CreateLevelMenu instance;

    private CreateLevelMenu() {

        this.txtNumOfQuestions = new Text("Количество вопросов:");
        this.sldrNumOfQuestions = new Slider(10, 100, 20);

        this.txtKeyType = new Text("Тип тональности:");
        this.rbMajor = new RadioButton("Мажор");
        this.rbMinor = new RadioButton("Минор");
        this.tgKeyType = new ToggleGroup();

        this.txtTonic = new Text("Тоника:");
        this.rbC = new RadioButton("До");
        this.rbNotC = new RadioButton("Другая");
        this.rbRandom = new RadioButton("Случайная");
        this.tgTonic = new ToggleGroup();
        this.sldrTonic = new Slider(1, 12, 4);

        this.txtTones = new Text("Ступени:");

        this.cbTone1 = new CheckBox();
        this.cbTone2 = new CheckBox();
        this.cbTone3 = new CheckBox();
        this.cbTone4 = new CheckBox();
        this.cbTone5 = new CheckBox();
        this.cbTone6 = new CheckBox();
        this.cbTone7 = new CheckBox();
        this.cbTone8 = new CheckBox();
        this.cbTone9 = new CheckBox();
        this.cbTone10 = new CheckBox();
        this.cbTone11 = new CheckBox();
        this.cbTone12 = new CheckBox();
        this.cbTone13 = new CheckBox();

        this.txtTone1 = new Text("1");
        this.txtToneB2 = new Text("b2");
        this.txtTone2 = new Text("2");
        this.txtToneB3 = new Text("b3");
        this.txtTone3 = new Text("3");
        this.txtTone4 = new Text("4");
        this.txtToneS4 = new Text("#4");
        this.txtTone5 = new Text("5");
        this.txtToneB6 = new Text("b6");
        this.txtTone6 = new Text("6");
        this.txtToneB7 = new Text("b7");
        this.txtTone7 = new Text("7");
        this.txtTone8 = new Text("1");

        this.gpTones = new GridPane();

        this.txtChordsEnabled = new Text("Ступени:");
        this.cbChord1 = new CheckBox("1");
        this.cbChord2 = new CheckBox("2");
        this.cbChord3 = new CheckBox("3");
        this.cbChord4 = new CheckBox("4");
        this.cbChord5 = new CheckBox("5");
        this.cbChord6 = new CheckBox("6");
        this.cbChord7 = new CheckBox("7");

        this.hbChordsEnabled = new HBox(cbChord1, cbChord2, cbChord3, cbChord4, cbChord5, cbChord6, cbChord7);

        this.txtInversions = new Text("Обращения:");
        this.rbInvOff = new RadioButton("Трезвучие");
        this.rbInv1st = new RadioButton("1ое (секстаккорд)");
        this.rbInv2nd = new RadioButton("2ое (квартсекстаккорд)");
        this.rbInvRandom = new RadioButton("Случайное");
        this.tgInversions = new ToggleGroup();

        this.btnCreateLevel = new Button("Создать");
        this.txtCreateError = new Text("Выберите ступени!");

        this.gpCreateLevel = new GridPane();
        this.scnCreateLevelMenu = new Scene(gpCreateLevel,
                Settings.getInstance().getWidth(), Settings.getInstance().getHeight());
    }

    public static CreateLevelMenu getInstance() {
        if (instance == null) {
            instance = new CreateLevelMenu();
        }

        return instance;
    }

    public Text getTxtNumOfQuestions() {
        return txtNumOfQuestions;
    }

    public Slider getSldrNumOfQuestions() {
        return sldrNumOfQuestions;
    }

    public Text getTxtKeyType() {
        return txtKeyType;
    }

    public RadioButton getRbMajor() {
        return rbMajor;
    }

    public RadioButton getRbMinor() {
        return rbMinor;
    }

    public ToggleGroup getTgKeyType() {
        return tgKeyType;
    }

    public Text getTxtTonic() {
        return txtTonic;
    }

    public RadioButton getRbC() {
        return rbC;
    }

    public RadioButton getRbNotC() {
        return rbNotC;
    }

    public RadioButton getRbRandom() {
        return rbRandom;
    }

    public ToggleGroup getTgTonic() {
        return tgTonic;
    }

    public Slider getSldrTonic() {
        return sldrTonic;
    }

    public Text getTxtTones() {
        return txtTones;
    }

    public CheckBox getCbTone1() {
        return cbTone1;
    }

    public CheckBox getCbTone2() {
        return cbTone2;
    }

    public CheckBox getCbTone3() {
        return cbTone3;
    }

    public CheckBox getCbTone4() {
        return cbTone4;
    }

    public CheckBox getCbTone5() {
        return cbTone5;
    }

    public CheckBox getCbTone6() {
        return cbTone6;
    }

    public CheckBox getCbTone7() {
        return cbTone7;
    }

    public CheckBox getCbTone8() {
        return cbTone8;
    }

    public CheckBox getCbTone9() {
        return cbTone9;
    }

    public CheckBox getCbTone10() {
        return cbTone10;
    }

    public CheckBox getCbTone11() {
        return cbTone11;
    }

    public CheckBox getCbTone12() {
        return cbTone12;
    }

    public CheckBox getCbTone13() {
        return cbTone13;
    }

    public Text getTxtTone1() {
        return txtTone1;
    }

    public Text getTxtToneB2() {
        return txtToneB2;
    }

    public Text getTxtTone2() {
        return txtTone2;
    }

    public Text getTxtToneB3() {
        return txtToneB3;
    }

    public Text getTxtTone3() {
        return txtTone3;
    }

    public Text getTxtTone4() {
        return txtTone4;
    }

    public Text getTxtToneS4() {
        return txtToneS4;
    }

    public Text getTxtTone5() {
        return txtTone5;
    }

    public Text getTxtToneB6() {
        return txtToneB6;
    }

    public Text getTxtTone6() {
        return txtTone6;
    }

    public Text getTxtToneB7() {
        return txtToneB7;
    }

    public Text getTxtTone7() {
        return txtTone7;
    }

    public Text getTxtTone8() {
        return txtTone8;
    }

    public GridPane getGpTones() {
        return gpTones;
    }

    public Text getTxtChordsEnabled() {
        return txtChordsEnabled;
    }

    public CheckBox getCbChord1() {
        return cbChord1;
    }

    public CheckBox getCbChord2() {
        return cbChord2;
    }

    public CheckBox getCbChord3() {
        return cbChord3;
    }

    public CheckBox getCbChord4() {
        return cbChord4;
    }

    public CheckBox getCbChord5() {
        return cbChord5;
    }

    public CheckBox getCbChord6() {
        return cbChord6;
    }

    public CheckBox getCbChord7() {
        return cbChord7;
    }

    public HBox getHbChordsEnabled() {
        return hbChordsEnabled;
    }

    public Text getTxtInversions() {
        return txtInversions;
    }

    public RadioButton getRbInvOff() {
        return rbInvOff;
    }

    public RadioButton getRbInv1st() {
        return rbInv1st;
    }

    public RadioButton getRbInv2nd() {
        return rbInv2nd;
    }

    public RadioButton getRbInvRandom() {
        return rbInvRandom;
    }

    public ToggleGroup getTgInversions() {
        return tgInversions;
    }

    public Button getBtnCreateLevel() {
        return btnCreateLevel;
    }

    public Text getTxtCreateError() {
        return txtCreateError;
    }

    public void setTxtCreateError(Text txtCreateError) {
        this.txtCreateError = txtCreateError;
    }

    public GridPane getGpCreateLevel() {
        return gpCreateLevel;
    }

    public Scene getScnCreateLevelMenu() {
        return scnCreateLevelMenu;
    }
}
