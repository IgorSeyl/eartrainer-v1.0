package main.models.scenes.other;

import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class HowToUse {

    private Text txtHowToUse;
    private GridPane gpHowToUse;
    private Scene scnHowToUseMenu;

    private static HowToUse instance;

    private HowToUse() {
        this.txtHowToUse = new Text(
                "Мелодия:\n" +
                        "\n" +
                        "1) До мажор - первая половина ступеней без хроматики\n" +
                        "2) До мажор - вторая половина ступеней без хроматики\n" +
                        "3) До мажор - полная октава без хроматики\n" +
                        "4) Другая тоника - полная октава без хроматики\n" +
                        "5) Случайная тоника - полная октава без хроматики\n" +
                        "6) Повторить шаги 1 - 5, добавив хроматику\n" +
                        "7) См. шаги 1 - 6, но в Миноре\n" +
                        "\n" +
                        "Гармония:\n" +
                        "\n" +
                        "1) 1, 4, и 5 ступени - без обращений\n" +
                        "2) Все ступени - без обращений\n" +
                        "3) Освоить обращения"

        );
        this.gpHowToUse = new GridPane();
        this.scnHowToUseMenu = new Scene(gpHowToUse, Settings.getInstance().getWidth(), Settings.getInstance().getHeight());
    }

    public static HowToUse getInstance() {
        if (instance == null) {
            instance = new HowToUse();
        }

        return instance;
    }

    public Text getTxtHowToUse() {
        return txtHowToUse;
    }

    public GridPane getGpHowToUse() {
        return gpHowToUse;
    }

    public Scene getScnHowToUseMenu() {
        return scnHowToUseMenu;
    }
}
