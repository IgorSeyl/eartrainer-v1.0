package main.models.scenes.other;

import javafx.scene.layout.GridPane;

public class Settings {

    private int width;
    private int height;
    private String cadence;
    private GridPane gpSettings;

    private static Settings instance;

    private Settings() {
        this.width = 800;
        this.height = 500;
        this.cadence = "I - IV - V - I";
        this.gpSettings = new GridPane();
    }

    public static Settings getInstance() {
        if (instance == null) instance = new Settings();
        return instance;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getCadence() {
        return cadence;
    }

    public void setCadence(String cadence) {
        this.cadence = cadence;
    }

    public GridPane getGpSettings() {
        return gpSettings;
    }
}
