package main.models.scenes.other;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class MainMenu {

    private Button btnHowToUse;
    private Button btnMelody;
    private Button btnHarmony;

    private GridPane gpMainMenu;
    private Scene scnMainMenu;

    private static MainMenu instance;

    private MainMenu() {
        this.btnHowToUse = new Button("Как заниматься");
        this.btnMelody = new Button("Мелодия");
        this.btnHarmony = new Button("Гармония");
        this.gpMainMenu = new GridPane();
        this.scnMainMenu = new Scene(gpMainMenu, Settings.getInstance().getWidth(), Settings.getInstance().getHeight());
    }

    public static MainMenu getInstance() {
        if (instance == null){
            instance = new MainMenu();
        }

        return instance;
    }

    public Button getBtnHowToUse() {
        return btnHowToUse;
    }

    public Button getBtnMelody() {
        return btnMelody;
    }

    public Button getBtnHarmony() {
        return btnHarmony;
    }

    public GridPane getGpMainMenu() {
        return gpMainMenu;
    }

    public Scene getScnMainMenu() {
        return scnMainMenu;
    }
}
