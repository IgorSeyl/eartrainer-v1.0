package main.models;

import javafx.stage.Stage;

public class PrimaryStage {

    private static Stage instance;

    private PrimaryStage() {
    }

    public static Stage getInstance(){
        if (instance == null){
            instance = new Stage();
        }

        return instance;
    }
}
