package main.enums;

public enum Tonic {
    C ("До"), NOT_C ("Другая"), RANDOM ("Случайная");

    private String tonic;

    Tonic(String tonic) {
        this.tonic = tonic;
    }

    public String getValue() {
        return tonic;
    }
}
