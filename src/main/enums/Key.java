package main.enums;

public enum Key {

    C ("C"),
    C_SHARP("C#"),
    D("D"),
    D_SHARP("D#"),
    E("E"),
    F("F"),
    F_SHARP("F#"),
    G("G"),
    G_SHARP("G#"),
    A("A"),
    A_SHARP("A#"),
    B("B");

    private String key;

    Key(String key) {
        this.key = key;
    }

    public String getValue() {
        return key;
    }
}
