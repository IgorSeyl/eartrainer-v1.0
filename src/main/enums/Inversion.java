package main.enums;

public enum Inversion {

    OFF ("Трезвучие"), FIRST("1ое (секстаккорд)"), SECOND("2ое (квартсекстаккорд)"), RANDOM("Случайное");

    private String Inversion;

    Inversion(String inversion) {
        Inversion = inversion;
    }

    public String getValue() {
        return Inversion;
    }
}
