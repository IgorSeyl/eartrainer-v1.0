package main;

import javafx.application.Application;
import javafx.stage.Stage;
import main.helpers.HelperFX;
import main.helpers.MyFX;
import main.models.PrevButton;
import main.models.PrimaryStage;
import main.models.scenes.levels.LevelMenu;
import main.models.scenes.other.HowToUse;
import main.models.scenes.other.MainMenu;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {

        primaryStage = PrimaryStage.getInstance();
        MainMenu mainMenu = MainMenu.getInstance();
        HowToUse howToUse = HowToUse.getInstance();
        LevelMenu levelMenu = LevelMenu.getInstance();

        MyFX.setBtnPrev();
        HelperFX.setButtonWidth(
                140,
                PrevButton.getInstance(),
                mainMenu.getBtnHowToUse(),
                mainMenu.getBtnMelody(),
                mainMenu.getBtnHarmony(),
                levelMenu.getBtnChooseLvl(),
                levelMenu.getBtnDeleteLvl(),
                levelMenu.getBtnCreateLevelMenu()
        );

        MyFX.setGridPanes();

        HelperFX.setHalignmentCenter(
                PrevButton.getInstance(),
                levelMenu.getBtnChooseLvl(),
                levelMenu.getBtnDeleteLvl(),
                levelMenu.getBtnCreateLevelMenu()
        );

        // --- Главное меню ---

        HelperFX.setInOneColumn(mainMenu.getGpMainMenu(),
                mainMenu.getBtnHowToUse(), mainMenu.getBtnMelody(), mainMenu.getBtnHarmony());

        // --- Как заниматься ---

        HelperFX.setInOneColumn(howToUse.getGpHowToUse(), howToUse.getTxtHowToUse());
        HelperFX.setSceneOnMouseClicked(mainMenu.getBtnHowToUse(), howToUse.getScnHowToUseMenu());

        // --- Выбор уровня ---

        HelperFX.setInOneColumn(
                levelMenu.getGpLevelMenu(),
                levelMenu.getTblLevels(),
                levelMenu.getBtnChooseLvl(),
                levelMenu.getBtnDeleteLvl(),
                levelMenu.getBtnCreateLevelMenu());

        HelperFX.setSceneOnMouseClicked(mainMenu.getBtnMelody(), levelMenu.getScnLevelMenu());
        HelperFX.setSceneOnMouseClicked(mainMenu.getBtnHarmony(), levelMenu.getScnLevelMenu());

        // --- Удаление уровня ---

        MyFX.setDeleteLevel();

        // --- Создание уровня ---

        MyFX.setCreateLevelMenu();
        MyFX.createLevel();

        // --- Уровень ---

        MyFX.setLevelPracticeMenu();
        MyFX.startLevel();
        MyFX.setRepeatButton();
        MyFX.setShowAnswerButton();
        MyFX.setAnsweringEvent();

        // --- Primary Stage ---

        primaryStage.setTitle("Ear Trainer");
        primaryStage.setScene(mainMenu.getScnMainMenu());
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
