package main.helpers;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import main.enums.Inversion;
import main.enums.Key;
import main.enums.Tonic;
import main.models.*;
import main.models.scenes.levels.CreateLevelMenu;
import main.models.scenes.levels.LevelMenu;
import main.models.scenes.levels.PracticeMenu;
import main.models.scenes.other.HowToUse;
import main.models.scenes.other.MainMenu;
import main.models.scenes.other.Settings;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MyFX {

    public static void setBtnPrev() {

        Button prevButton = PrevButton.getInstance();
        Stage primaryStage = PrimaryStage.getInstance();
        MainMenu mainMenu = MainMenu.getInstance();

        prevButton.setOnMouseClicked(event -> {

            for (int i = 0; i < Sound.getInstance().getPlayers().size(); i++) {

                Sound.getInstance().getPlayers().get(i).stop();

            }

            primaryStage.setScene(mainMenu.getScnMainMenu());
        });
    }

    public static void setGridPanes() {

        CreateLevelMenu.getInstance().getGpCreateLevel().setAlignment(Pos.CENTER);
        CreateLevelMenu.getInstance().getGpCreateLevel().setVgap(10);
        CreateLevelMenu.getInstance().getGpCreateLevel().setHgap(10);

        LevelMenu.getInstance().getGpLevelMenu().setAlignment(Pos.CENTER);
        LevelMenu.getInstance().getGpLevelMenu().setVgap(10);
        LevelMenu.getInstance().getGpLevelMenu().setHgap(10);

        PracticeMenu.getInstance().getGpPracticeMenu().setAlignment(Pos.CENTER);
        PracticeMenu.getInstance().getGpPracticeMenu().setVgap(10);
        PracticeMenu.getInstance().getGpPracticeMenu().setHgap(10);

        HowToUse.getInstance().getGpHowToUse().setAlignment(Pos.CENTER);
        HowToUse.getInstance().getGpHowToUse().setVgap(10);
        HowToUse.getInstance().getGpHowToUse().setHgap(10);

        MainMenu.getInstance().getGpMainMenu().setAlignment(Pos.CENTER);
        MainMenu.getInstance().getGpMainMenu().setVgap(10);
        MainMenu.getInstance().getGpMainMenu().setHgap(10);

    }

    public static void fillLevelTable() throws JAXBException {

        Check check = Check.getInstance();
        TableView<Level> tableView = LevelMenu.getInstance().getTblLevels();
        TableColumn<Level, String> tonic = new TableColumn<>("Тоника");
        TableColumn<Level, String> keyType = new TableColumn<>("Лад");
        TableColumn<Level, Integer> qNum = new TableColumn<>("Кол-во\n" + "вопросов");

        ObservableList<Level> levelsList = FXCollections.observableArrayList();
        File[] levels;

        String path = Helper.getFileFromResources("resources/levels").getPath();

        if (check.isMelody()) levels = new File(Helper.checkOS(path + "/melody")).listFiles();
        else levels = new File(Helper.checkOS(path + "/harmony")).listFiles();

        if (levels != null) {

            for (File level : levels) {

                Level unmarshalledLevel = Helper.unmarshall(level);
                unmarshalledLevel.setFilePath(level.getPath());
                levelsList.add(unmarshalledLevel);
            }

            tableView.setItems(levelsList);

            tonic.setCellValueFactory(cellData -> {

                String valTonic;
                if (check.isMelody()) valTonic = cellData.getValue().getTonic().getValue();
                else valTonic = cellData.getValue().getKey().getValue();

                return new ReadOnlyStringWrapper(valTonic);
            });

            keyType.setCellValueFactory(cellData -> {

                boolean isMinor = cellData.getValue().isMinor();
                String booleanAsString;
                if (isMinor) booleanAsString = "Минор";
                else booleanAsString = "Мажор";

                return new ReadOnlyStringWrapper(booleanAsString);
            });

            qNum.setCellValueFactory(new PropertyValueFactory<>("numOfQuestions"));

            TableColumn<Level, String> enabledDegree = new TableColumn<>("Ступени");
            enabledDegree.setCellValueFactory(cellData -> {
                if (cellData.getValue().getEnabledDegrees().contains("s4")) {
                    cellData.getValue().getEnabledDegrees().remove("s4");
                    cellData.getValue().getEnabledDegrees().add("#4");
                }

                return new ReadOnlyStringWrapper(HelperFX.sortDegrees(cellData.getValue()));
            });

            tableView.getColumns().clear();

            if (check.isMelody()) tableView.getColumns().addAll(tonic, keyType, qNum, enabledDegree);
            else {

                TableColumn<Level, String> enabledInversions = new TableColumn<>("Обращения");
                enabledInversions.setCellValueFactory(new PropertyValueFactory<>("enabledInversions"));

                enabledInversions.setCellValueFactory(cellData -> {

                    String valInversion = cellData.getValue().getEnabledInversions().getValue();
                    return new ReadOnlyStringWrapper(valInversion);
                });

                tableView.getColumns().addAll(qNum, enabledDegree, enabledInversions, keyType, tonic);
            }
        }

        tableView.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        tableView.getColumns().stream().forEach((column) ->
        {
            Text t = new Text(column.getText());
            double max = t.getLayoutBounds().getWidth();

            for (int i = 0; i < tableView.getItems().size(); i++) {
                if (column.getCellData(i) != null) {
                    t = new Text(column.getCellData(i).toString());
                    double calcwidth = t.getLayoutBounds().getWidth();
                    if (calcwidth > max) {
                        max = calcwidth;
                    }
                }
            }
            column.setPrefWidth(max + 20.0d);
        });
    }

    public static void setDeleteLevel() {

        LevelMenu levelMenu = LevelMenu.getInstance();
        Check check = Check.getInstance();
        String path = Helper.getFileFromResources("resources").getPath();

        levelMenu.getBtnDeleteLvl().setOnMouseClicked(event -> {

            TableView tblLevels = levelMenu.getTblLevels();

            int selectedRow = tblLevels.getSelectionModel().getSelectedIndex();

            Level selectedLevel = (Level) tblLevels.getSelectionModel().getSelectedItem();

            File level;
            File result;

            if (check.isMelody()) {

                String fileName = selectedLevel.getNumOfQuestions() + "q " +
                        selectedLevel.getTonic() + " " +
                        (selectedLevel.isMinor() ? "minor" : "major") + " " +
                        (selectedLevel.getEnabledDegrees().size() == 13 ?
                                "[s4, b2, 1, b3, 2, 3, 4, b6, 5, b7, 6, 7, 1+]" :
                                String.valueOf(selectedLevel.getEnabledDegrees()).replace("#", "s")); // ???


                level = new File(Helper.checkOS(path + "/levels/melody/") + fileName + ".xml");
                result = new File(Helper.checkOS(path + "/results/melody/") + fileName + ".ser");

            } else {

                String fileName = selectedLevel.getNumOfQuestions() + "q " +
                        selectedLevel.getEnabledDegrees() + " " +
                        selectedLevel.getEnabledInversions() + " " +
                        (selectedLevel.isMinor() ? "minor" : "major") + " " +
                        selectedLevel.getTonic();

                level = new File(Helper.checkOS(path + "/levels/harmony/") + fileName + ".xml");
                result = new File(Helper.checkOS(path + "/results/harmony/") + fileName + ".ser");
            }

            Optional levelToDelete = tblLevels.getItems().stream().filter(lvl -> {

                Level myLvl = (Level) lvl;
                return myLvl.getFilePath().equals(level.getPath());
            }).findFirst();

            if (levelToDelete.isPresent()) tblLevels.getItems().remove(levelToDelete.get());

            level.delete();
            result.delete();

            tblLevels.requestFocus();
            tblLevels.getSelectionModel().select(selectedRow);
        });
    }

    public static void setCreateLevelMenu() {

        LevelMenu levelMenu = LevelMenu.getInstance();
        CreateLevelMenu createLevelMenu = CreateLevelMenu.getInstance();
        Check check = Check.getInstance();

        HelperFX.setHalignmentRight(
                createLevelMenu.getTxtNumOfQuestions(),
                createLevelMenu.getTxtTonic(),
                createLevelMenu.getTxtTones()
        );

        HelperFX.setHalignmentCenter(
                createLevelMenu.getBtnCreateLevel(),
                createLevelMenu.getTxtCreateError()
        );

        HelperFX.setButtonWidth(
                140,
                createLevelMenu.getBtnCreateLevel()
        );

        createLevelMenu.getSldrNumOfQuestions().setMinWidth(Settings.getInstance().getWidth() / 3);
        createLevelMenu.getSldrNumOfQuestions().setShowTickMarks(true);
        createLevelMenu.getSldrNumOfQuestions().setShowTickLabels(true);
        createLevelMenu.getSldrNumOfQuestions().setBlockIncrement(10);
        createLevelMenu.getSldrNumOfQuestions().setMajorTickUnit(10);
        createLevelMenu.getSldrNumOfQuestions().setMinorTickCount(0);
        createLevelMenu.getSldrNumOfQuestions().setSnapToTicks(true);

        createLevelMenu.getRbMajor().setToggleGroup(createLevelMenu.getTgKeyType());
        createLevelMenu.getRbMinor().setToggleGroup(createLevelMenu.getTgKeyType());
        createLevelMenu.getTgKeyType().selectToggle(createLevelMenu.getRbMajor());

        createLevelMenu.getRbInvOff().setToggleGroup(createLevelMenu.getTgInversions());
        createLevelMenu.getRbInv1st().setToggleGroup(createLevelMenu.getTgInversions());
        createLevelMenu.getRbInv2nd().setToggleGroup(createLevelMenu.getTgInversions());
        createLevelMenu.getRbInvRandom().setToggleGroup(createLevelMenu.getTgInversions());
        createLevelMenu.getTgInversions().selectToggle(createLevelMenu.getRbInvOff());

        createLevelMenu.getRbC().setToggleGroup(createLevelMenu.getTgTonic());
        createLevelMenu.getRbNotC().setToggleGroup(createLevelMenu.getTgTonic());
        createLevelMenu.getRbRandom().setToggleGroup(createLevelMenu.getTgTonic());
        createLevelMenu.getTgTonic().selectToggle(createLevelMenu.getRbC());

        createLevelMenu.getSldrTonic().setMinWidth(Settings.getInstance().getWidth() / 3);
        createLevelMenu.getSldrTonic().setShowTickMarks(true);
        createLevelMenu.getSldrTonic().setShowTickLabels(true);
        createLevelMenu.getSldrTonic().setBlockIncrement(1);
        createLevelMenu.getSldrTonic().setMajorTickUnit(1);
        createLevelMenu.getSldrTonic().setMinorTickCount(0);
        createLevelMenu.getSldrTonic().setSnapToTicks(true);

        createLevelMenu.getSldrTonic().setLabelFormatter(new StringConverter<Double>() {
            @Override
            public String toString(Double n) {
                if (n == 1) return "A";
                if (n == 2) return "A#";
                if (n == 3) return "B";
                if (n == 5) return "C#";
                if (n == 6) return "D";
                if (n == 7) return "D#";
                if (n == 8) return "E";
                if (n == 9) return "F";
                if (n == 10) return "F#";
                if (n == 11) return "G";
                if (n == 12) return "G#";

                return "C";
            }

            @Override
            public Double fromString(String s) {
                switch (s) {
                    case "A":
                        return 1d;
                    case "A#":
                        return 2d;
                    case "B":
                        return 3d;
                    case "C#":
                        return 5d;
                    case "D":
                        return 6d;
                    case "D#":
                        return 7d;
                    case "E":
                        return 8d;
                    case "F":
                        return 9d;
                    case "F#":
                        return 10d;
                    case "G":
                        return 11d;
                    case "G#":
                        return 12d;

                    default:
                        return 4d;

                }
            }
        });

        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone1(), 0, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone2(), 1, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone3(), 2, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone4(), 3, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone5(), 4, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone6(), 6, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone7(), 7, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone8(), 8, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone9(), 9, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone10(), 10, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone11(), 11, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone12(), 12, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getCbTone13(), 14, 1);

        createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone1(), 1, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB2(), 2, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone2(), 3, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB3(), 4, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone3(), 5, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone4(), 7, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneS4(), 8, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone5(), 9, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB6(), 10, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone6(), 11, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB7(), 12, 0);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone7(), 13, 1);
        createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone8(), 15, 1);

        createLevelMenu.getTgKeyType().selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {

                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getCbTone4());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getCbTone5());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getCbTone9());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getCbTone10());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getCbTone11());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getCbTone12());

                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getTxtToneB3());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getTxtTone3());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getTxtToneB6());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getTxtTone6());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getTxtToneB7());
                createLevelMenu.getGpTones().getChildren().remove(createLevelMenu.getTxtTone7());

                if (createLevelMenu.getTgKeyType().getSelectedToggle() == createLevelMenu.getRbMajor()) {

                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone4(), 3, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone5(), 4, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone9(), 9, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone10(), 10, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone11(), 11, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone12(), 12, 1);

                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB3(), 4, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone3(), 5, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB6(), 10, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone6(), 11, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB7(), 12, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone7(), 13, 1);

                } else {

                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone4(), 4, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone5(), 5, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone9(), 10, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone10(), 11, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone11(), 12, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getCbTone12(), 13, 0);

                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB3(), 5, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone3(), 6, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB6(), 11, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone6(), 12, 0);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtToneB7(), 13, 1);
                    createLevelMenu.getGpTones().add(createLevelMenu.getTxtTone7(), 14, 0);
                }
            }
        });

        levelMenu.getBtnCreateLevelMenu().setOnMouseClicked(event -> {

            if (check.isMelody()) MyFX.setCreateMelodyLevelMenu();
            else MyFX.setCreateHarmonyLevelMenu();
            HelperFX.setScene(createLevelMenu.getScnCreateLevelMenu());


        });

    }

    private static void setCreateMelodyLevelMenu() {

        CreateLevelMenu createLevelMenu = CreateLevelMenu.getInstance();

        createLevelMenu.getGpCreateLevel().getChildren().clear();

        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getTxtNumOfQuestions(), 0, 0);
        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getSldrNumOfQuestions(), 1, 0, 4, 1);

        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getTxtKeyType(), 0, 1);
        createLevelMenu.getGpCreateLevel().add(
                new HBox(10, createLevelMenu.getRbMajor(), createLevelMenu.getRbMinor()),
                1, 1, 3, 1);
        HelperFX.setHalignmentRight(createLevelMenu.getTxtKeyType());

        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getTxtTonic(), 0, 2);
        createLevelMenu.getGpCreateLevel().add(
                new HBox(10, createLevelMenu.getRbC(), createLevelMenu.getRbNotC(), createLevelMenu.getRbRandom()),
                1, 2, 5, 1);

        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getTxtTones(), 0, 4, 1, 2);
        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getGpTones(), 1, 4, 3, 2);

        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getBtnCreateLevel(), 0, 8, 5, 1);

        createLevelMenu.getTxtCreateError().setVisible(false);
        createLevelMenu.getTxtCreateError().setFill(Paint.valueOf("red"));
        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getTxtCreateError(), 0, 9, 5, 1);
    }

    private static void setCreateHarmonyLevelMenu() {

        CreateLevelMenu createLevelMenu = CreateLevelMenu.getInstance();

        createLevelMenu.getGpCreateLevel().getChildren().clear();

        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getTxtNumOfQuestions(), 0, 0);
        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getSldrNumOfQuestions(), 0, 1, 4, 1);

        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getTxtChordsEnabled(), 0, 2);
        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getHbChordsEnabled(), 0, 3, 4, 1);

        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getTxtInversions(), 0, 4);
        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getRbInvOff(), 0, 5);
        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getRbInv1st(), 1, 5);
        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getRbInv2nd(), 2, 5);
        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getRbInvRandom(), 3, 5);

        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getTxtKeyType(), 0, 6);
        HelperFX.setHalignmentLeft(createLevelMenu.getTxtKeyType());
        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getRbMajor(), 0, 7);
        createLevelMenu.getGpCreateLevel().add(createLevelMenu.getRbMinor(), 1, 7);

        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getTxtTonic(), 0, 8);
        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getSldrTonic(), 0, 9, 4, 1);

        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getBtnCreateLevel(), 0, 10, 5, 1);

        createLevelMenu.getTxtCreateError().setVisible(false);
        createLevelMenu.getTxtCreateError().setFill(Paint.valueOf("red"));
        createLevelMenu.getGpCreateLevel().add(
                createLevelMenu.getTxtCreateError(), 0, 11, 5, 1);
    }

    public static void createLevel() {

        CreateLevelMenu createLevelMenu = CreateLevelMenu.getInstance();
        Check check = Check.getInstance();

        createLevelMenu.getBtnCreateLevel().setOnMouseClicked(event -> {

            try {
                if (check.isMelody()) {

                    Tonic chosenTonic = Tonic.C;
                    if (createLevelMenu.getRbNotC().isSelected()) chosenTonic = Tonic.NOT_C;
                    if (createLevelMenu.getRbRandom().isSelected()) chosenTonic = Tonic.RANDOM;

                    HashSet<String> selectedTones = new HashSet<>();
                    if (createLevelMenu.getCbTone1().isSelected()) selectedTones.add("1");
                    if (createLevelMenu.getCbTone2().isSelected()) selectedTones.add("b2");
                    if (createLevelMenu.getCbTone3().isSelected()) selectedTones.add("2");
                    if (createLevelMenu.getCbTone4().isSelected()) selectedTones.add("b3");
                    if (createLevelMenu.getCbTone5().isSelected()) selectedTones.add("3");
                    if (createLevelMenu.getCbTone6().isSelected()) selectedTones.add("4");
                    if (createLevelMenu.getCbTone7().isSelected()) selectedTones.add("s4");
                    if (createLevelMenu.getCbTone8().isSelected()) selectedTones.add("5");
                    if (createLevelMenu.getCbTone9().isSelected()) selectedTones.add("b6");
                    if (createLevelMenu.getCbTone10().isSelected()) selectedTones.add("6");
                    if (createLevelMenu.getCbTone11().isSelected()) selectedTones.add("b7");
                    if (createLevelMenu.getCbTone12().isSelected()) selectedTones.add("7");
                    if (createLevelMenu.getCbTone13().isSelected()) selectedTones.add("1+"); // 1oct

                    Level level = new Level(
                            (int) createLevelMenu.getSldrNumOfQuestions().getValue(),
                            chosenTonic,
                            createLevelMenu.getRbMinor().isSelected(),
                            selectedTones
                    );

                    if (selectedTones.isEmpty())
                        createLevelMenu.getTxtCreateError().setVisible(true);
                    else {
                        createLevelMenu.getTxtCreateError().setVisible(false);
                        Helper.marshall(level);
                        MyFX.fillLevelTable();
                        HelperFX.setScene(LevelMenu.getInstance().getScnLevelMenu());
                    }

                } else {

                    HashSet<String> enabledChords = new HashSet<>();
                    if (createLevelMenu.getCbChord1().isSelected()) enabledChords.add("1");
                    if (createLevelMenu.getCbChord2().isSelected()) enabledChords.add("2");
                    if (createLevelMenu.getCbChord3().isSelected()) enabledChords.add("3");
                    if (createLevelMenu.getCbChord4().isSelected()) enabledChords.add("4");
                    if (createLevelMenu.getCbChord5().isSelected()) enabledChords.add("5");
                    if (createLevelMenu.getCbChord6().isSelected()) enabledChords.add("6");
                    if (createLevelMenu.getCbChord7().isSelected()) enabledChords.add("7");

                    Inversion enabledInversions = null;
                    if (createLevelMenu.getRbInvOff().isSelected()) enabledInversions = Inversion.OFF;
                    if (createLevelMenu.getRbInv1st().isSelected()) enabledInversions = Inversion.FIRST;
                    if (createLevelMenu.getRbInv2nd().isSelected()) enabledInversions = Inversion.SECOND;
                    if (createLevelMenu.getRbInvRandom().isSelected()) enabledInversions = Inversion.RANDOM;

                    Key chosenTonic = null;
                    if (createLevelMenu.getSldrTonic().getValue() == 1d) chosenTonic = Key.A;
                    if (createLevelMenu.getSldrTonic().getValue() == 2d) chosenTonic = Key.A_SHARP;
                    if (createLevelMenu.getSldrTonic().getValue() == 3d) chosenTonic = Key.B;
                    if (createLevelMenu.getSldrTonic().getValue() == 4d) chosenTonic = Key.C;
                    if (createLevelMenu.getSldrTonic().getValue() == 5d) chosenTonic = Key.C_SHARP;
                    if (createLevelMenu.getSldrTonic().getValue() == 6d) chosenTonic = Key.D;
                    if (createLevelMenu.getSldrTonic().getValue() == 7d) chosenTonic = Key.D_SHARP;
                    if (createLevelMenu.getSldrTonic().getValue() == 8d) chosenTonic = Key.E;
                    if (createLevelMenu.getSldrTonic().getValue() == 9d) chosenTonic = Key.F;
                    if (createLevelMenu.getSldrTonic().getValue() == 10d) chosenTonic = Key.F_SHARP;
                    if (createLevelMenu.getSldrTonic().getValue() == 11d) chosenTonic = Key.G;
                    if (createLevelMenu.getSldrTonic().getValue() == 12d) chosenTonic = Key.G_SHARP;

                    Level level = new Level(
                            (int) createLevelMenu.getSldrNumOfQuestions().getValue(),
                            enabledChords,
                            enabledInversions,
                            createLevelMenu.getRbMinor().isSelected(),
                            chosenTonic
                    );

                    if (enabledChords.isEmpty())
                        createLevelMenu.getTxtCreateError().setVisible(true);

                    else {
                        createLevelMenu.getTxtCreateError().setVisible(false);
                        Helper.marshall(level);
                        MyFX.fillLevelTable();
                        HelperFX.setScene(LevelMenu.getInstance().getScnLevelMenu());
                    }

                }

            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });
    }

    public static void setLevelPracticeMenu() {

        PracticeMenu practiceMenu = PracticeMenu.getInstance();
        ArrayList<Button> buttons = practiceMenu.getButtons();
        ArrayList<Rectangle> rectangles = practiceMenu.getRectangles();
        GridPane gpPracticeMenu = practiceMenu.getGpPracticeMenu();

        practiceMenu.getBpRepeatAndAnswer().setLeft(practiceMenu.getBtnRepeat());
        practiceMenu.getBpRepeatAndAnswer().setRight(practiceMenu.getBtnShowAnswer());
        HelperFX.setButtonWidth(150, practiceMenu.getBtnRepeat(), practiceMenu.getBtnShowAnswer());

        gpPracticeMenu.add(practiceMenu.getBtn1(), 0, 3);
        gpPracticeMenu.add(practiceMenu.getRectangle1(), 0, 4);
        gpPracticeMenu.add(practiceMenu.getBtn2(), 1, 3);
        gpPracticeMenu.add(practiceMenu.getRectangle2(), 1, 4);
        gpPracticeMenu.add(practiceMenu.getBtn4(), 3, 3);
        gpPracticeMenu.add(practiceMenu.getRectangle4(), 3, 4);
        gpPracticeMenu.add(practiceMenu.getBtn5(), 4, 3);
        gpPracticeMenu.add(practiceMenu.getRectangle5(), 4, 4);

        HelperFX.setHalignmentCenter(
                practiceMenu.getTxtCurrentAndMaxQ(),
                practiceMenu.getTxtAnswer(),
                practiceMenu.getTxtCorrectAndIncorrectQ()
        );

        gpPracticeMenu.add(practiceMenu.getTxtCurrentAndMaxQ(), 0, 5, 2, 1);

        practiceMenu.getTxtAnswer().setOpacity(0);

        buttons.forEach(button -> HelperFX.setButtonWidth(37.5, button));

        rectangles.forEach(rectangle -> rectangle.setOpacity(0));
    }

    private static void fillLevelPracticeMenu() {

        PracticeMenu practiceMenu = PracticeMenu.getInstance();
        ArrayList<Button> buttons = practiceMenu.getButtons();
        GridPane gpPracticeMenu = practiceMenu.getGpPracticeMenu();
        Check check = Check.getInstance();
        TableView<Level> tblLevels = LevelMenu.getInstance().getTblLevels();
        TableView.TableViewSelectionModel<Level> selectionModel = tblLevels.getSelectionModel();

        practiceMenu.setLevel(new AtomicReference<>(selectionModel.getSelectedItem()));
        Level level = practiceMenu.getLevel().get();

        practiceMenu.setCurrentQ(1);
        practiceMenu.setNumOfCorrectQ(0);
        practiceMenu.setNumOfIncorrectQ(0);
        check.setIncorrectAnswer(new AtomicBoolean(false));

        practiceMenu.setRandomDegree(practiceMenu.getLevel().get().getEnabledDegrees());
        if (check.isMelody()) practiceMenu.setRandomKey(level.getTonic());
        if (!check.isMelody()) practiceMenu.setRandomInversion(level.getEnabledInversions());

        buttons.forEach(button -> button.setVisible(false));

        buttons.stream() //
                .filter(button -> level.getEnabledDegrees().contains(button.getText())) //
                .forEach(button -> button.setVisible(true)); //

        if (level.getEnabledDegrees().contains("1+")) practiceMenu.getBtn8().setVisible(true);
        else practiceMenu.getBtn8().setVisible(false);

        gpPracticeMenu.getChildren().remove(practiceMenu.getBtnB3());
        gpPracticeMenu.getChildren().remove(practiceMenu.getRectangleB3());
        gpPracticeMenu.getChildren().remove(practiceMenu.getBtn3());
        gpPracticeMenu.getChildren().remove(practiceMenu.getRectangle3());
        gpPracticeMenu.getChildren().remove(practiceMenu.getBtnB6());
        gpPracticeMenu.getChildren().remove(practiceMenu.getRectangleB6());
        gpPracticeMenu.getChildren().remove(practiceMenu.getBtn6());
        gpPracticeMenu.getChildren().remove(practiceMenu.getRectangle6());
        gpPracticeMenu.getChildren().remove(practiceMenu.getBtnB7());
        gpPracticeMenu.getChildren().remove(practiceMenu.getRectangleB7());
        gpPracticeMenu.getChildren().remove(practiceMenu.getBtn7());
        gpPracticeMenu.getChildren().remove(practiceMenu.getRectangle7());
        gpPracticeMenu.getChildren().remove(practiceMenu.getBtn8());
        gpPracticeMenu.getChildren().remove(practiceMenu.getRectangle8());

        gpPracticeMenu.getChildren().remove(practiceMenu.getBpRepeatAndAnswer());
        gpPracticeMenu.getChildren().remove(practiceMenu.getTxtCorrectAndIncorrectQ());
        gpPracticeMenu.getChildren().remove(practiceMenu.getTxtAnswer());

        if (check.isMelody()) {
            gpPracticeMenu.add(practiceMenu.getBpRepeatAndAnswer(), 0, 0, 8, 1);
            gpPracticeMenu.add(practiceMenu.getBtn8(), 7, 3);
            gpPracticeMenu.add(practiceMenu.getRectangle8(), 7, 4);
            gpPracticeMenu.add(
                    practiceMenu.getTxtCorrectAndIncorrectQ(), 6, 5, 2, 1);
            gpPracticeMenu.add(practiceMenu.getTxtAnswer(), 0, 5, 8, 1);

        } else {

            gpPracticeMenu.add(practiceMenu.getBpRepeatAndAnswer(), 0, 0, 7, 1);
            gpPracticeMenu.getChildren().remove(practiceMenu.getBtn8());
            gpPracticeMenu.getChildren().remove(practiceMenu.getRectangle8());
            gpPracticeMenu.add(
                    practiceMenu.getTxtCorrectAndIncorrectQ(), 5, 5, 2, 1);
            gpPracticeMenu.add(practiceMenu.getTxtAnswer(), 0, 5, 7, 1);
        }

        if (level.isMinor() && check.isMelody()) {

            HBox hbBlackKeys1 = new HBox(practiceMenu.getBtnB2());
            hbBlackKeys1.setAlignment(Pos.CENTER);

            HBox hbRectangles1 = new HBox(practiceMenu.getRectangleB2());
            hbRectangles1.setAlignment(Pos.CENTER);

            HBox hbBlackKeys2 = new HBox(10, practiceMenu.getBtn3(), practiceMenu.getBtnS4());
            hbBlackKeys2.setAlignment(Pos.CENTER);

            HBox hbRectangles2 = new HBox(10, practiceMenu.getRectangle3(), practiceMenu.getRectangleS4());
            hbRectangles2.setAlignment(Pos.CENTER);

            HBox hbBlackKeys3 = new HBox(10, practiceMenu.getBtn6(), practiceMenu.getBtn7());
            hbBlackKeys3.setAlignment(Pos.CENTER);

            HBox hbRectangles3 = new HBox(10, practiceMenu.getRectangle6(), practiceMenu.getRectangle7());
            hbRectangles3.setAlignment(Pos.CENTER);

            gpPracticeMenu.add(hbBlackKeys1, 0, 2, 2, 1);
            gpPracticeMenu.add(hbRectangles1, 0, 1, 2, 1);
            gpPracticeMenu.add(hbBlackKeys2, 2, 2, 3, 1);
            gpPracticeMenu.add(hbRectangles2, 2, 1, 3, 1);
            gpPracticeMenu.add(hbBlackKeys3, 5, 2, 3, 1);
            gpPracticeMenu.add(hbRectangles3, 5, 1, 3, 1);

            gpPracticeMenu.add(practiceMenu.getBtnB3(), 2, 3);
            gpPracticeMenu.add(practiceMenu.getRectangleB3(), 2, 4);
            gpPracticeMenu.add(practiceMenu.getBtnB6(), 5, 3);
            gpPracticeMenu.add(practiceMenu.getRectangleB6(), 5, 4);
            gpPracticeMenu.add(practiceMenu.getBtnB7(), 6, 3);
            gpPracticeMenu.add(practiceMenu.getRectangleB7(), 6, 4);

        } else {

            HBox hbBlackKeys1 = new HBox(10, practiceMenu.getBtnB2(), practiceMenu.getBtnB3());
            hbBlackKeys1.setAlignment(Pos.CENTER);

            HBox hbRectangles1 = new HBox(10, practiceMenu.getRectangleB2(), practiceMenu.getRectangleB3());
            hbRectangles1.setAlignment(Pos.CENTER);

            HBox hbBlackKeys2 = new HBox(
                    10, practiceMenu.getBtnS4(), practiceMenu.getBtnB6(), practiceMenu.getBtnB7());
            hbBlackKeys2.setAlignment(Pos.CENTER);

            HBox hbRectangles2 = new HBox(
                    10, practiceMenu.getRectangleS4(), practiceMenu.getRectangleB6(), practiceMenu.getRectangleB7());
            hbRectangles2.setAlignment(Pos.CENTER);

            gpPracticeMenu.add(hbBlackKeys1, 0, 2, 3, 1);
            gpPracticeMenu.add(hbRectangles1, 0, 1, 3, 1);
            gpPracticeMenu.add(hbBlackKeys2, 3, 2, 4, 1);
            gpPracticeMenu.add(hbRectangles2, 3, 1, 4, 1);

            gpPracticeMenu.add(practiceMenu.getBtn3(), 2, 3);
            gpPracticeMenu.add(practiceMenu.getRectangle3(), 2, 4);
            gpPracticeMenu.add(practiceMenu.getBtn6(), 5, 3);
            gpPracticeMenu.add(practiceMenu.getRectangle6(), 5, 4);
            gpPracticeMenu.add(practiceMenu.getBtn7(), 6, 3);
            gpPracticeMenu.add(practiceMenu.getRectangle7(), 6, 4);

        }
    }

    public static void startLevel() {

        Check check = Check.getInstance();
        LevelMenu levelMenu = LevelMenu.getInstance();
        PracticeMenu practiceMenu = PracticeMenu.getInstance();

        levelMenu.getBtnChooseLvl().setOnMouseClicked(event -> {

            if (!levelMenu.getTblLevels().getSelectionModel().isEmpty()){

                fillLevelPracticeMenu();

                try {

                    Result.getInstance().getResult().clear();
                    Helper.deserializeResult();
                    HelperFX.setScene(practiceMenu.getScnPracticeMenu());

                    if (check.isMelody()) {

                        System.out.println(
                                "Key: " + practiceMenu.getRandomKey()[0].getValue() +
                                        ", Chromatic degree: " + practiceMenu.getRandomDegree().get());

                        Sound.play(
                                Helper.wavPathCadence(practiceMenu.getRandomKey()[0]),
                                Helper.wavPathDegree(practiceMenu.getRandomKey()[0], practiceMenu.getRandomDegree().get())
                        );

                    } else {

                        System.out.println(
                                "Degree: " + practiceMenu.getRandomDegree().get() +
                                        ", Inversion: " + practiceMenu.getRandomInversion());

                        Sound.play(
                                Helper.wavPathDegree(practiceMenu.getLevel().get().getKey(), 1, 1),
                                Helper.wavPathDegree(
                                        practiceMenu.getLevel().get().getKey(),
                                        practiceMenu.getRandomDegree().get(),
                                        practiceMenu.getRandomInversion().get()
                                )
                        );
                    }

                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void setRepeatButton() {

        Check check = Check.getInstance();
        PracticeMenu practiceMenu = PracticeMenu.getInstance();

        practiceMenu.getBtnRepeat().setOnMouseClicked(event -> {

            Key key = check.isMelody() ? practiceMenu.getRandomKey()[0] : practiceMenu.getLevel().get().getKey();
            Integer degree = practiceMenu.getRandomDegree().get();
            Integer inversion = practiceMenu.getRandomInversion().get();

            if (check.isMelody()) {
                Sound.play(
                        Helper.wavPathCadence(key),
                        Helper.wavPathDegree(key, degree)
                );

            } else {

                Sound.play(
                        Helper.wavPathDegree(key, 1, inversion),
                        Helper.wavPathDegree(key, degree, inversion)
                );
            }

        });
    }

    public static void setShowAnswerButton() {

        PracticeMenu practiceMenu = PracticeMenu.getInstance();
        Check check = Check.getInstance();

        practiceMenu.getBtnShowAnswer().setOnMouseClicked(event -> {

            try {

                int prevDegree = practiceMenu.getRandomDegree().get();
                Button button = practiceMenu.getButtons().get(prevDegree - 1);

                if (!check.isMelody()) {

                    for (int i = 0; i < practiceMenu.getButtons().size(); i++) {

                        if (practiceMenu.getButtons().get(i).getText().equals(
                                String.valueOf(practiceMenu.getRandomDegree().get())) &&
                                practiceMenu.getButtons().get(i).isVisible()
                        ) {
                            prevDegree = i + 1;
                            button = practiceMenu.getButtons().get(i);
                            break;
                        }
                    }
                }

                check.setIncorrectAnswer(new AtomicBoolean(true));

                MyFX.setResult(button);
                MyFX.playCorrectAnswer();
                if (practiceMenu.getCurrentQ().get() != practiceMenu.getMaxQ()) HelperFX.reveal(prevDegree);

                practiceMenu.increaseCurrentQ();
                practiceMenu.increaseNumOfIncorrectQ();

                if (practiceMenu.getCurrentQ().get() > practiceMenu.getMaxQ()) MyFX.finishLevel();

                check.setIncorrectAnswer(new AtomicBoolean(false));

                System.out.println(Result.getInstance().getResult());

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static void setAnsweringEvent() {

        PracticeMenu practiceMenu = PracticeMenu.getInstance();
        Check check = Check.getInstance();
        ArrayList<Button> buttons = practiceMenu.getButtons();

        for (int i = 0; i < buttons.size(); i++) {

            int finalI = i;

            buttons.get(i).setOnMouseClicked(event -> {

                Button button = buttons.get(finalI);
                String buttonText = button.getText();
                int buttonDegree = finalI + 1;
                int expectedDegree = finalI + 1;

                String buttonTextSubstring = button.getText().substring(0, 1);
                if (!(buttonTextSubstring.equals("b") || buttonTextSubstring.equals("#")) && !check.isMelody())
                    expectedDegree = Integer.parseInt(buttonText);

                boolean degreeMatch = practiceMenu.getRandomDegree().get() == expectedDegree;
                if (
                        (practiceMenu.getRandomDegree().get() == 1 || practiceMenu.getRandomDegree().get() == 13)
                                &&
                                (expectedDegree == 1 || expectedDegree == 13)

                ) degreeMatch = true;

                try {

                    if (practiceMenu.getCurrentQ().get() == practiceMenu.getMaxQ()) {

                        if (degreeMatch) {

                            practiceMenu.setTxtAnswer(true);
                            MyFX.setResult(button);
                            MyFX.playCorrectAnswer();

                            if (check.getIncorrectAnswer().get()) practiceMenu.increaseNumOfIncorrectQ();
                            else practiceMenu.increaseNumOfCorrectQ();

                            MyFX.finishLevel();

                        } else {
                            practiceMenu.setTxtAnswer(false);
                            check.setIncorrectAnswer(new AtomicBoolean(true));
                        }

                    } else {

                        if (degreeMatch) {

                            practiceMenu.setTxtAnswer(true);
                            MyFX.setResult(button);
                            MyFX.playCorrectAnswer();
                            HelperFX.reveal(buttonDegree);

                            practiceMenu.increaseCurrentQ();
                            if (check.getIncorrectAnswer().get()) practiceMenu.increaseNumOfIncorrectQ();
                            else practiceMenu.increaseNumOfCorrectQ();

                            check.setIncorrectAnswer(new AtomicBoolean(false));

                        } else {

                            practiceMenu.setTxtAnswer(false);
                            check.setIncorrectAnswer(new AtomicBoolean(true));
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private static void setResult(Button button) {

        Check check = Check.getInstance();
        Result result = Result.getInstance();
        String degree = button.getText();

        if (!check.getIncorrectAnswer().get()) {

            if (!result.getResult().containsKey(degree)) {

                result.setResult(degree, 1, 1);

            } else {

                result.setResult(
                        degree,
                        result.getResult().get(degree).getKey() + 1,
                        result.getResult().get(degree).getValue() + 1);
            }

        } else {

            if (!result.getResult().containsKey(degree)) {

                result.setResult(degree, 0, 1);

            } else {

                result.setResult(
                        degree,
                        result.getResult().get(degree).getKey(),
                        result.getResult().get(degree).getValue() + 1);
            }
        }
    }

    private static void playCorrectAnswer() {

        PracticeMenu practiceMenu = PracticeMenu.getInstance();
        Check check = Check.getInstance();
        Level level = practiceMenu.getLevel().get();

        Key harmonyKey = level.getKey();
        Key prevKey = practiceMenu.getRandomKey()[0];
        if (check.isMelody() && level.getTonic().equals(Tonic.RANDOM)) practiceMenu.setRandomKey(level.getTonic());
        Key newKey = practiceMenu.getRandomKey()[0];

        Integer prevDegree = practiceMenu.getRandomDegree().get();
        practiceMenu.setRandomDegree(level.getEnabledDegrees());
        Integer newDegree = practiceMenu.getRandomDegree().get();

        Integer prevInversion = practiceMenu.getRandomInversion().get();
        if (!check.isMelody() && level.getEnabledInversions().equals(Inversion.RANDOM))
            practiceMenu.setRandomInversion(level.getEnabledInversions());
        Integer newInversion = practiceMenu.getRandomInversion().get();

        if (practiceMenu.getCurrentQ().get() == practiceMenu.getMaxQ()) {

            if (Check.getInstance().isMelody()) {

                Sound.play(Helper.wavPathResolving(prevKey, prevDegree));

            } else {

                Sound.play(Helper.wavPathDegree(harmonyKey, prevDegree, prevInversion));
            }

        } else {

            if (Check.getInstance().isMelody()) {

                Sound.play(
                        Helper.wavPathResolving(prevKey, prevDegree),
                        Helper.wavPathCadence(newKey),
                        Helper.wavPathDegree(newKey, newDegree)
                );

                System.out.println(
                        "Key: " + newKey.getValue() + ", Chromatic degree: " + newDegree);

            } else {

                if (practiceMenu.getCurrentQ().get() != practiceMenu.getMaxQ()) {

                    Sound.play(
                            Helper.wavPathDegree(harmonyKey, prevDegree, prevInversion),
                            Helper.wavPathDegree(harmonyKey, newDegree, newInversion)
                    );

                    System.out.println(
                            "Degree: " + practiceMenu.getRandomDegree().get() +
                                    ", Inversion: " + practiceMenu.getRandomInversion());

                } else Sound.play(Helper.wavPathDegree(harmonyKey, prevDegree, prevInversion));

            }
        }
    }

    private static void finishLevel() throws IOException {

        ArrayList<Button> buttons = PracticeMenu.getInstance().getButtons();
        Result result = Result.getInstance();

        Helper.serializeResult();

        StringBuilder sbResult = new StringBuilder();

        for (int j = 0; j < buttons.size() - 1; j++) {

            if (result.getResult().containsKey(buttons.get(j).getText())) {

                int successPercent =(result.getResult().get(buttons.get(j).getText()).getKey() * 100) /
                        result.getResult().get(buttons.get(j).getText()).getValue();

                sbResult.
                        append(buttons.get(j).getText()).append(" ступень: ").
                        append(result.getResult().get(buttons.get(j).getText()).getKey()).
                        append(" из ").
                        append(result.getResult().get(buttons.get(j).getText()).getValue()).
                        append(" (" + successPercent + "%)").
                        append("\n");
            }
        }

        PrimaryStage.getInstance().setScene(MainMenu.getInstance().getScnMainMenu());
        Dialog<Result> finalResult = new Dialog<>();
        finalResult.setTitle("Результат");
        finalResult.setContentText(sbResult.toString());

        final ButtonType buttonOk = new ButtonType("ОК", ButtonBar.ButtonData.OK_DONE);
        finalResult.getDialogPane().getButtonTypes().add(buttonOk);
        finalResult.show();
    }
}
