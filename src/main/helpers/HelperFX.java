package main.helpers;

import javafx.animation.FadeTransition;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import main.models.Check;
import main.models.Level;
import main.models.PrevButton;
import main.models.PrimaryStage;
import main.models.scenes.levels.PracticeMenu;
import main.models.scenes.other.MainMenu;

import javax.xml.bind.JAXBException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class HelperFX {

    public static void addButton(Button newButton, GridPane gridPane, int colIndex, int rowIndex) {

        if (!gridPane.getChildren().contains(newButton)) gridPane.add(newButton, colIndex, rowIndex);
    }


    public static void addButton(Button newButton, GridPane gridPane,
                                 int colIndex, int rowIndex,
                                 int colSpan, int rowSpan) {

        if (!gridPane.getChildren().contains(newButton)) gridPane.add(newButton, colIndex, rowIndex, colSpan, rowSpan);
    }

    public static void setHalignmentCenter(Node... nodes) {

        for (Node node :
                nodes) {
            GridPane.setHalignment(node, HPos.CENTER);

        }
    }

    public static void setHalignmentRight(Node... nodes) {

        for (Node node :
                nodes) {
            GridPane.setHalignment(node, HPos.RIGHT);

        }
    }

    public static void setHalignmentLeft(Node... nodes) {

        for (Node node :
                nodes) {
            GridPane.setHalignment(node, HPos.LEFT);

        }
    }

    public static void setButtonWidth(double width, Button... buttons) {

        for (Button button :
                buttons) {
            button.setMinWidth(width);
            button.setMaxWidth(width);
        }
    }

    public static int getNumberOfRows(GridPane gridPane) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Method method = gridPane.getClass().getDeclaredMethod("getNumberOfRows");
        method.setAccessible(true);
        return (int) method.invoke(gridPane);
    }

    public static int getNumberOfColumns(GridPane gridPane) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Method method = gridPane.getClass().getDeclaredMethod("getNumberOfColumns");
        method.setAccessible(true);
        return (int) method.invoke(gridPane);
    }

    public static void setInOneColumn(GridPane gridPane, Node... nodes) {

        for (int i = 0; i < nodes.length; i++) {
            gridPane.add(nodes[i], 0, i);
        }
    }

    public static void setSceneOnMouseClicked(Button clickedButton, Scene scene) {

        Check check = Check.getInstance();
        MainMenu mainMenu = MainMenu.getInstance();

        clickedButton.setOnMouseClicked(event -> {

            try {

                if (clickedButton.equals(mainMenu.getBtnMelody())) {
                    check.setMelody(true);
                    MyFX.fillLevelTable();
                }
                if (clickedButton.equals(mainMenu.getBtnHarmony())) {
                    check.setMelody(false);
                    MyFX.fillLevelTable();
                }

            } catch (JAXBException e) {
                e.printStackTrace();
            }

            HelperFX.setScene(scene);
        });
    }

    public static void setScene(Scene scene) {

        Button prevButton = PrevButton.getInstance();
        Stage primaryStage = PrimaryStage.getInstance();

        try {

            HelperFX.addButton(
                    prevButton, (GridPane) scene.getRoot(),
                    0, getNumberOfRows((GridPane) scene.getRoot()),
                    getNumberOfColumns((GridPane) scene.getRoot()), getNumberOfRows((GridPane) scene.getRoot())
            );

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }

        primaryStage.setScene(scene);

    }

    public static String sortDegrees(Level level) {

        HashSet<String> enabledDegrees = level.getEnabledDegrees();
        ArrayList<String> result = new ArrayList<>();

        if (enabledDegrees.contains("1")) result.add("1");
        if (enabledDegrees.contains("b2")) result.add("b2");
        if (enabledDegrees.contains("2")) result.add("2");
        if (enabledDegrees.contains("b3")) result.add("b3");
        if (enabledDegrees.contains("3")) result.add("3");
        if (enabledDegrees.contains("4")) result.add("4");
        if (enabledDegrees.contains("#4")) result.add("#4");
        if (enabledDegrees.contains("5")) result.add("5");
        if (enabledDegrees.contains("b6")) result.add("b6");
        if (enabledDegrees.contains("6")) result.add("6");
        if (enabledDegrees.contains("b7")) result.add("b7");
        if (enabledDegrees.contains("7")) result.add("7");
        if (enabledDegrees.contains("1+")) result.add("1+");

        return result.toString().replace("[", "").replace("]", "");
    }

    public static void reveal(Node node, int revealDuration, int hideDuration) {

        FadeTransition fdReveal = new FadeTransition(Duration.millis(revealDuration), node);

        fdReveal.setFromValue(0);
        fdReveal.setToValue(1);
        fdReveal.play();

        fdReveal.setOnFinished(event -> {
            FadeTransition fdHide = new FadeTransition(Duration.millis(hideDuration), node);
            fdHide.setFromValue(1);
            fdHide.setToValue(0);
            fdHide.play();
        });
    }

    public static void reveal(int degree) {

        int longSound = 500;
        int shortSound = 250;

        boolean isMinor = PracticeMenu.getInstance().getLevel().get().isMinor();
        boolean isMelody = Check.getInstance().isMelody();
        ArrayList<Rectangle> rectangles = PracticeMenu.getInstance().getRectangles();
        ArrayList<FadeTransition> fdReveals = new ArrayList<>();
        ArrayList<FadeTransition> fdHides = new ArrayList<>();

        ArrayList<Integer> rectanglesIndexes = new ArrayList<>();

        if (isMelody) {

            switch (degree) {
                case 1:
                    rectanglesIndexes = new ArrayList<>();
                    rectanglesIndexes.add(0);
                    break;
                case 2:
                    rectanglesIndexes = new ArrayList<>(Arrays.asList(1, 0));
                    break;
                case 3:
                    rectanglesIndexes = new ArrayList<>(Arrays.asList(2, 0));
                    break;
                case 4:
                    rectanglesIndexes = new ArrayList<>(Arrays.asList(3, 2, 0));
                    break;
                case 5:
                    rectanglesIndexes = isMinor ?
                            new ArrayList<>(Arrays.asList(4, 3, 2, 0)) :
                            new ArrayList<>(Arrays.asList(4, 2, 0));
                    break;
                case 6:
                    rectanglesIndexes = isMinor ?
                            new ArrayList<>(Arrays.asList(5, 3, 2, 0)) :
                            new ArrayList<>(Arrays.asList(5, 4, 2, 0));
                    break;
                case 7:
                    rectanglesIndexes = isMinor ?
                            new ArrayList<>(Arrays.asList(6, 7, 8, 10, 12)) :
                            new ArrayList<>(Arrays.asList(6, 7, 9, 11, 12));
                    break;
                case 8:
                    rectanglesIndexes = isMinor ?
                            new ArrayList<>(Arrays.asList(7, 8, 10, 12)) :
                            new ArrayList<>(Arrays.asList(7, 9, 11, 12));
                    break;
                case 9:
                    rectanglesIndexes = isMinor ?
                            new ArrayList<>(Arrays.asList(8, 10, 12)) :
                            new ArrayList<>(Arrays.asList(8, 9, 11, 12));
                    break;
                case 10:
                    rectanglesIndexes = isMinor ?
                            new ArrayList<>(Arrays.asList(9, 10, 12)) :
                            new ArrayList<>(Arrays.asList(9, 11, 12));
                    break;
                case 11:
                    rectanglesIndexes = isMinor ?
                            new ArrayList<>(Arrays.asList(10, 12)) :
                            new ArrayList<>(Arrays.asList(10, 11, 12));
                    break;
                case 12:
                    rectanglesIndexes = new ArrayList<>(Arrays.asList(11, 12));
                    break;
                default:
                    rectanglesIndexes = new ArrayList<>();
                    rectanglesIndexes.add(12);
                    break;
            }

        } else rectanglesIndexes.add(degree - 1);

        for (int i = 0; i < rectanglesIndexes.size(); i++) {

            FadeTransition addReveal = new FadeTransition();
            addReveal.setNode(rectangles.get(rectanglesIndexes.get(i)));
            addReveal.setFromValue(0);
            addReveal.setToValue(1);

            FadeTransition addHide = new FadeTransition();
            addHide.setNode(rectangles.get(rectanglesIndexes.get(i)));
            addHide.setFromValue(1);
            addHide.setToValue(0);

            if (i == 0 || i == rectangles.size() - 1) {

                addReveal.setDuration(Duration.millis(longSound * 0.75));
                addHide.setDuration(Duration.millis(longSound * 0.25));

            } else {

                addReveal.setDuration(Duration.millis(shortSound * 0.75));
                addHide.setDuration(Duration.millis(shortSound * 0.25));
            }

            fdReveals.add(addReveal);
            fdHides.add(addHide);
        }

        fdReveals.get(0).play();

        for (int i = 0; i < fdReveals.size(); i++) {

            int finalI = i;

            fdReveals.get(finalI).setOnFinished(event -> {
                fdHides.get(finalI).play();
            });

            if (finalI != fdReveals.size() - 1) {
                fdHides.get(finalI).setOnFinished(event -> {
                    fdReveals.get(finalI + 1).play();
                });
            }
        }
    }
}
