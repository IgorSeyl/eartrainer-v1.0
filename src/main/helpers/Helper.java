package main.helpers;

import main.enums.Key;
import main.models.Check;
import main.models.Level;
import main.models.Result;
import main.models.scenes.levels.PracticeMenu;
import main.models.scenes.other.Settings;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.HashMap;

public class Helper {

    public static File getFileFromResources(String path) {

        return new File(checkOS(System.getProperty("user.dir") + "/" + path));
    }

    public static void marshall(Level level) {

        Check check = Check.getInstance();
        String path = Helper.getFileFromResources("resources/levels").getPath();

        try {
            JAXBContext jaxbCtxt = JAXBContext.newInstance(Level.class);
            Marshaller jaxbMarshaller = jaxbCtxt.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            if (check.isMelody()) {

                jaxbMarshaller.marshal(level, new File(
                                checkOS(
                                        path + "/melody/" +
                                                level.getNumOfQuestions() + "q " +
                                                level.getTonic() + " " +
                                                (level.isMinor() ? "minor" : "major") + " " +
                                                level.getEnabledDegrees() + ".xml"
                                )
                        )
                );

            } else {

                jaxbMarshaller.marshal(level, new File(
                        checkOS(
                                path + "/harmony/" +
                                        level.getNumOfQuestions() + "q " +
                                        level.getEnabledDegrees() + " " +
                                        level.getEnabledInversions() + " " +
                                        (level.isMinor() ? "minor" : "major") + " " +
                                        level.getTonic() + ".xml"
                        )
                ));
            }

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static Level unmarshall(File xml) throws JAXBException {

        Level level = new Level();

        JAXBContext jaxbCtxt = JAXBContext.newInstance(Level.class);
        Unmarshaller jaxbUnmarshaller = jaxbCtxt.createUnmarshaller();

        level = (Level) jaxbUnmarshaller.unmarshal(xml);

        return level;
    }

    public static void serializeResult() throws IOException {

        Level level = PracticeMenu.getInstance().getLevel().get();
        Check check = Check.getInstance();
        String path = Helper.getFileFromResources("resources/results").getPath();

        FileOutputStream fos;

        if (check.isMelody()) {

            fos = new FileOutputStream(
                    checkOS(
                            path + "/melody/" +
                                    level.getNumOfQuestions() + "q " +
                                    level.getTonic() + " " +
                                    (level.isMinor() ? "minor" : "major") + " " +
                                    level.getEnabledDegrees() + ".ser"
                    )
            );

        } else {

            fos = new FileOutputStream(
                    checkOS(
                            path + "/harmony/" +
                                    level.getNumOfQuestions() + "q " +
                                    level.getEnabledDegrees() + " " +
                                    level.getEnabledInversions() + " " +
                                    (level.isMinor() ? "minor" : "major") + " " +
                                    level.getTonic() + ".ser"
                    )
            );
        }

        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(Result.getInstance().getResult());
        oos.close();
        fos.close();
    }

    public static void deserializeResult() throws IOException, ClassNotFoundException {

        Level level = PracticeMenu.getInstance().getLevel().get();
        Check check = Check.getInstance();
        String path = Helper.getFileFromResources("resources/results").getPath();

        String resultFilePath;

        if (check.isMelody()) resultFilePath =
                checkOS(
                        path + "/melody/" +
                                level.getNumOfQuestions() + "q " +
                                level.getTonic() + " " +
                                (level.isMinor() ? "minor" : "major") + " " +
                                level.getEnabledDegrees() + ".ser"
                );

        else resultFilePath =
                checkOS(
                        path + "/harmony/" +
                                level.getNumOfQuestions() + "q " +
                                level.getEnabledDegrees() + " " +
                                level.getEnabledInversions() + " " +
                                (level.isMinor() ? "minor" : "major") + " " +
                                level.getTonic() + ".ser"
                );

        boolean fileExist = new File(resultFilePath).exists();

        if (fileExist) {

            HashMap map = null;

            FileInputStream fis = new FileInputStream(resultFilePath);
            ObjectInputStream ois = new ObjectInputStream(fis);
            map = (HashMap) ois.readObject();
            ois.close();
            fis.close();

            Result.getInstance().setResult(map);
        }

    }

    public static int getRandom(int min, int max) {

        return min + (int) (Math.random() * ((max - min) + 1));
    }

    public static String[] checkOS(String[] string) {

        if (System.getProperty("os.name").equals("Windows")) {

            for (int i = 0; i < string.length; i++) {
                string[i] = string[i].replaceAll("/", "\\");
            }
        }

        return string;
    }

    public static String checkOS(String string) {

        if (System.getProperty("os.name").equals("Windows"))
            string = string.replaceAll("/", "\\");

        return string;
    }

    public static String wavPathCadence(Key key) {
        String path = Helper.getFileFromResources("resources/records").getPath();

        String keyType = PracticeMenu.getInstance().getLevel().get().isMinor() ? "minor" : "major";
        String cadenceType = Settings.getInstance().getCadence();

        return path + "/melody/" + key.getValue() + " " + keyType + "/cadence/" + cadenceType + ".wav";
    }

    public static String wavPathDegree(Key key, Integer wavDegree) {

        return Helper.wavPath(key, "notes", wavDegree);
    }

    public static String wavPathDegree(Key key, Integer wavDegree, Integer wavInversion) {

        return Helper.wavPath(key, wavDegree, wavInversion);
    }

    public static String wavPathResolving(Key key, Integer wavInteger) {

        return Helper.wavPath(key, "resolving", wavInteger);
    }

    private static String wavPath(Key key, String soundType, Integer wavDegree) {
        String path = Helper.getFileFromResources("resources/records").getPath();

        String recordsPath = path + "/melody/";
        String keyType = PracticeMenu.getInstance().getLevel().get().isMinor() ? "minor" : "major";
        File[] sounds = new File(recordsPath + key.getValue() + " " + keyType + "/" + soundType).listFiles();
        String wavFile = wavDegree.toString();

        assert sounds != null;
        for (File sound : sounds) {

            if (wavFile.equals(sound.getName().substring(0, sound.getName().indexOf(" ")))) {
                wavFile = sound.getName();
                break;
            }
        }

        return recordsPath + key.getValue() + " " + keyType + "/" + soundType + "/" + wavFile;
    }

    private static String wavPath(Key key, Integer wavDegree, Integer wavInversion) {
        String path = Helper.getFileFromResources("resources/records").getPath();

        String recordsPath = path + "/harmony/";
        String keyType = PracticeMenu.getInstance().getLevel().get().isMinor() ? "minor" : "major";
        String wavFile = wavDegree.toString() + " " + wavInversion.toString();

        return recordsPath + key.getValue() + " " + keyType + "/" + wavFile + ".wav";
    }
}
